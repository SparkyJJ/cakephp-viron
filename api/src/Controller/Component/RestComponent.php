<?php

namespace App\Controller\Component;

use Cake\Controller\Component;

/**
 * Rest component
 *
 * @property \Cake\Controller\Component\RequestHandlerComponent $RequestHandler
 */
class RestComponent extends Component
{
    /**
     * Other Components this component uses.
     *
     * @var array
     */
    public $components = [
        'RequestHandler',
    ];

    /**
     * Set success response
     *
     * @param array  $data    Response data. Defaults to empty array.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    public function respondSuccess($data = [], $message = '')
    {
        return $this->setResponse(200, $data, $message);
    }

    /**
     * Set validation failed response
     *
     * @param array  $data    Response data. Defaults to empty array.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    public function respondValidationFailed($data = [], $message = '')
    {
        return $this->setResponse(400, $data, $message);
    }

    /**
     * Set internal server error response
     *
     * @param array  $data    Response data. Defaults to empty array.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    public function respondInternalServerError($data = [], $message = '')
    {
        $message = $message ?: __('ERROR-MSG-INTERNAL-ERROR');

        return $this->setResponse(500, $data, $message);
    }

    /**
     * Set Forbidden response
     *
     * @param array  $data    Response data. Defaults to empty array.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    public function respondForbidden($data = [], $message = '')
    {
        $message = $message ?: __('ERROR-MSG-FORBIDDEN');

        return $this->setResponse(403, $data, $message);
    }

    /** Set Not Found response
     *
     * @param array  $data    Response data. Defaults to empty array.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    public function respondNotfound($data = [], $message = '')
    {
        $message = $message ?: __('ERROR-MSG-NOT-FOUND');

        return $this->setResponse(404, $data, $message);
    }

    /** Respond JSON data
     *
     * @param array  $data    Response data. Defaults to empty array.
     * @param int    $status  HTTP status code. Defaults to 200.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    public function respondJson($data = [], $status = 200, $message = '')
    {
        $controller = $this->getController();
        $json = json_encode($data, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES );
        $response = $controller
            ->getResponse()
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($status, $message)
            ->withStringBody($json);

        $this->RequestHandler->renderAs($controller, 'json');

        return $response;
    }

    /**
     * Set response
     *
     * @param int    $status  HTTP status code. Defaults to 200.
     * @param array  $data    Response data. Defaults to empty array.
     * @param string $message Response message. Defaults to empty string.
     *
     * @return null
     */
    private function setResponse($status = 200, $data = [], $message = '')
    {
        $controller = $this->getController();

        $response = $controller
            ->getResponse()
            ->withStatus($status, $message);

        $controller
            ->setResponse($response)
            ->set(
                [
                    'status' => $response->getStatusCode(),
                    'message' => $response->getReasonPhrase(),
                    'data' => $data,
                    '_serialize' => ['status', 'message', 'data'],
                ]
            );

        $this->RequestHandler->renderAs($controller, 'json');

        return null;
    }
}
