<?php

namespace App\Controller;

use App\Controller\AppController;

/**
 * Viron Controller
 */
class VironController extends AppController
{
    public function getAuthType() {
        $authType = [
            [
              'type' => 'email',
              'provider' => 'cakephp',
              'url' => '/login',
              'method' => 'POST',
            ],
            [
              'type' => 'signout',
              'provider' => '',
              'url' => '/logout',
              'method' => 'POST',
            ],
        ];

        return $this->Rest->respondJson($authType);
    }
}
