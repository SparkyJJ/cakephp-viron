<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */
use Cake\Http\Middleware\CsrfProtectionMiddleware;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 * Cache: Routes are cached to improve performance, check the RoutingMiddleware
 * constructor in your `src/Application.php` file to change this behavior.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    // Register scoped middleware for in scopes.
    $routes->registerMiddleware('csrf', new CsrfProtectionMiddleware([
        'httpOnly' => true
    ]));

    /**
     * Apply a middleware to the current route scope.
     * Requires middleware to be registered via `Application::routes()` with `registerMiddleware()`
     */
    $routes->applyMiddleware('csrf');


    $routes->post('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->post('/logout', ['controller' => 'Users', 'action' => 'logout']);

    $routes->get('/account', ['controller' => 'Users', 'action' => 'get-account']);
    $routes->put('/account/:user_id', ['controller' => 'Users', 'action' => 'update-account'])
        ->setPatterns(['user_id' => '\d+'])
        ->setPass(['user_id']);

    $routes->get('/viron', ['controller' => 'Viron', 'action' => 'get-pages']);
    $routes->get('/viron_authtype', ['controller' => 'Viron', 'action' => 'get-auth-type']);
    $routes->get('/swagger.json', ['controller' => 'Viron', 'action' => 'get-swagger']);

    $routes->scope('/admin', function (RouteBuilder $routes) {
        $routes->get('/audit-log', ['controller' => 'Admin', 'action' => 'get-audit-log']);

        $routes->scope('/user', function (RouteBuilder $routes) {
            $routes->get('/', ['controller' => 'Admin', 'action' => 'get-user-list']);
            $routes->post('/', ['controller' => 'Admin', 'action' => 'add-user']);
            $routes->put('/:user_id', ['controller' => 'Admin', 'action' => 'update-user'])
                ->setPatterns(['user_id' => '\d+'])
                ->setPass(['user_id']);
            $routes->delete('/:user_id', ['controller' => 'Admin', 'action' => 'delete-user'])
                ->setPatterns(['user_id' => '\d+'])
                ->setPass(['user_id']);
        });

        $routes->scope('/role', function (RouteBuilder $routes) {
            $routes->get('/', ['controller' => 'Admin', 'action' => 'get-role-list']);
            $routes->post('/', ['controller' => 'Admin', 'action' => 'add-role']);
            $routes->put('/:role_id', ['controller' => 'Admin', 'action' => 'update-role'])
                ->setPatterns(['role_id' => '\d+'])
                ->setPass(['role_id']);
            $routes->delete('/:role_id', ['controller' => 'Admin', 'action' => 'delete-role'])
                ->setPatterns(['role_id' => '\d+'])
                ->setPass(['role_id']);
        });
    });

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *
     * ```
     * $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);
     * $routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);
     * ```
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
     */
    $routes->fallbacks(DashedRoute::class);
});

/**
 * If you need a different set of middleware or none at all,
 * open new scope and define routes there.
 *
 * ```
 * Router::scope('/api', function (RouteBuilder $routes) {
 *     // No $routes->applyMiddleware() here.
 *     // Connect API actions here.
 * });
 * ```
 */
